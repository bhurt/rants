\documentclass{article}

\title{Laziness is the Right Default}
\author{Brian Hurt}
\date{May, 2024}

\begin{document}

\maketitle

\begin{abstract}
While those ivory tower academics can play around with their toy
strictly evaluated languages, us real programmers need our lazy
evaluation.
\end{abstract}

\section{Introduction}

It is commonly held opinion among many (most?) Haskell developers that,
had we had it all to do over again knowing what we know now, it would
have been better for Haskell to have been strictly evaluated, rather
than lazily evaluated.  The purity, immutability, monads, and all the
other consequences of laziness we'd keep- just ditch the actual
laziness.  Even Simon Peyton Jones has expressed this opinion.

I'd like to express a contrarian opinion- that laziness is the right
default for a programming language, {\em especially} for a
production-oriented engineering language.  Simon Peyton Jones may have
thought he had made an error, but it turns out he was mistaken.  This is
not to say that errors weren't made in the design of Haskell.  They
were, and Haskell is not a perfect language.  Don't get me started on
exceptions.  Just that laziness as the default was not one of them.  My
opinion is that what people want is a much more aggressive strictness
analyzer, not a strict language.

\section{Laziness Enables Optimization}

Consider the code:

\begin{verbatim}
    map f (map g lst)
\end{verbatim}

Can we rewrite that code as:

\begin{verbatim}
    map (f . g) lst
\end{verbatim}

or not?  

This is just one of a number of such code transformations known
alternatively as deforrestation or fusing.  We are replacing two
traversals of the data structure with one.

And this is an important optimization.  Now we've exposed the expression
\verb|f . g|, which is ripe for inlining and further optimizations.  If
nothing else, we've eliminated the cost of allocating the intermediate
data structure.

The problem here is: do the two different peices of code behave the
same, for all functions \verb|f| and \verb|g|, and all lists
\verb|list|?

With strictly evaluated code, the answer is sometimes not.  Consider:

\begin{verbatim}
    f :: Int -> Int
    f x
        | x == 3    = throw ExceptionA
        | otherwise =  x

    g :: Int -> Int
    g x
        | x == 7    = throw ExceptionB
        | otherwise = x

    lst :: [ Int ]
    lst = [ 1 .. 10 ]
\end{verbatim}

With these definitions, a strictly evaluated language would say this
code:

\begin{verbatim}
    map f (map g lst)
\end{verbatim}

throws ExceptionB, while this code:

\begin{verbatim}
    map (f . g) lst
\end{verbatim}

throws ExceptionA.  Applying the optimization changes the behavior of
the program, in this case.  Attempting to determine if it is safe, in a
strict language, to perform this transform leads pretty quickly to
needing to solve the halting problem.  Most compilers for most languages
don't even try.

But with lazy evaluation, this transform is always correct.  For all
functions \verb|f|.  For all functions \verb|g|.  For all lists.  In the
above case, both functions throw ExceptionA.  The compiler doesn't need
to know anything about the functions being called or the list being
operated on- if it sees a map of a map, it can fuse it.  And GHC does.

This sort of high-level optimization is of very great importance in real
world code.  It's very easy for the \verb|map g lst| to be in one
module, and the the \verb|map f| to be in a different module maintained
by different people.  And thus no human ever notices that the fusion
could happen.  Or maybe they do notice, but the increased complexity to
export what is needed for the programmer to perform the fusion by hand
makes it not worthwhile to do.  Remember that in the real world, the
only constant is that the requirements will change.  And thus the
implementation will need to change.  Oh, sure- {\em today} that code is
returning \verb|map g lst|, and thus the fusion can occur.  Tommorrow,
who knows?  Maybe it won't.  This is why good APIs don't expose {\em
how} things are done, only {\em what} is done.

I have a hunch (not backed up by any data so feel free to disbelieve),
that real-world Haskell code is actually faster than real-world C/C++
code.  This is a spicy hot take, to say the least, but it comes from
exactly this feature.  We ``know'' that C/C++ is fast, because the
benchmarks tell us they are\footnote{And, because C/C++ code tends to be
complicated and hard to modify, and since optimized code is complicated
and hard to modify, all complicated and hard to modify code must be
optimized, right?  Socrates is human, I am human, therefor I am
Socrates, right?}.  But benchmarks are, by their nature, very
artificial.  The requirements are precise and tend not to change (as
opposed to real world requirements which are never precise, and where
the only constant is change).  And the whole program can be rewritten,
from scratch, in a short period of time, like an afternoon (as opposed
to real world programs).  So needing to apply fusing by hand is no
disadvantage in a benchmark.  But it is a huge disadvantage in a real
world program.

That being said, I would actually argue that performance is less of a
issue than many programmers make it out to be.  Consider for a moment
the popularity of languages like Python, Ruby, and Javascript- all which
are complete diasters from a pure performance standpoint.  Those
developers choosing those languages simply value developer productivity
over performance.  As should we.  It makes economic sense- clock cycles
are cheap.  Developer time is expensive in the extreme.

Especially given that, in most programs, less than 1\% of the code
consumes 99+\% of the CPU cycles.  Why is Python, a famously slow
programming language, so popular in AI, an area famous for being CPU
bound?  Because the core loops in the core libraries have been rewritten
in C for high performance.  A similar argument can be made for laziness
and Haskell.

\section{Laziness Simplifies APIs}

I believe that people understand things best when you start with the
specific and concrete, and then, once they have several examples in
mind, then introduce the abstract and the general.  So I want to start
with specific examples, and then pull the general concepts out later.
So let me start with three examples (of many, many, possible examples),
and then draw some conclusions.

\subsection{Sort and Take}

Let's say that I want to find the $k$ smallest elements of a list.  Now,
I could write some complex data structure and algorithm to do this, or I
could, in Haskell, just go:

\begin{verbatim}
    kmin :: Ord a => Int -> [a] -> [a]
    kmin k = take k . sort
\end{verbatim}

This works, because \verb|sort| is a lazy merge sort.  Which means that
the cost to deternmine the first element is $O(N)$, and the cost of
determining each additional element is $O(\log(N))$.  If you were to
require all $N$ elements, this would be $O(N\log(N))$ cost- but as I
only need the first $k$ elements, this cost is $O(N + k\log(N))$.  If
this is not asymptocially optimal, it's very close.  I don't need to pay
to sort the remaining $N - k$ elements, and I don't.

I want to note here that the functions \verb|sort| and \verb|take| need
to be implemented for other reasons.  This will become important later.

\subsection{Picking the Result}

Let's say I define a common four-function calculator:

\begin{verbatim}
    data Expr =
        Const Double
        | Var
        | Add Expr Expr
        | Sub Expr Expr
        | Mul Expr Expr
        | Div Expr Expr
\end{verbatim}

Now, sometimes I want to simply evaluate an expression- given a value
for \verb|Var|, return the result.  But sometimes, what I want is the
derivative of the expression.  And sometimes I want both.  And maybe
it'll turn out I don't need either.  But the key point here is that at
compile time I don't know which it is that I need.

What I can do, in Haskell, is just write one function that calculates
both the result and the derivative:

\begin{verbatim}
    eval :: Expr -> Double -> (Double, Double)
    eval _ (Const x) = (x, 0.0)
    eval x Var       = (x, 1.0)
    eval x (Add p q) =
        let (f, f') = eval x p
            (g, g') = eval x q
        in
        (f + g, f' + g')
    -- etc.
\end{verbatim}

I produce both the result and the derivative, and then only pay for the
cost of actually computing only those values I need.

\subsection{Stopping a Fold}

Let's say I want to rewrite the find function.  Yes, I am aware
that the correct implementation of this is to just import the find
function from Data.List, I'm using it as an example of a common problem.
Again, with laziness, this is easy:

\begin{verbatim}
    find :: Foldable t => (a -> Bool) -> t a -> Maybe a
    find f = foldr go Nothing
        where
            go :: a -> Maybe a -> Maybe a
            go x rest
                | f x       -> Just x
                | otherwise -> rest
\end{verbatim}

Here, the \verb|rest| variable is the result from the rest of the list.
The trick here is that I only evaluate it- I only pay the computational
cost- if I need it.  Effectively, I can choose at every element whether
to continue evaluating down the list, or stop here.

Have you ever noticed how complicated looping gets in imperative
languages, to support all the various possible cases?  I mean, you start
with for, while, and do/while, then add for-in (for (i in lst) ...),
then break and continue are added, then named (labeled) breaks and
continues allowing you to break out of or continue multiple loops, and
so on.  We've replaced all that complicated looping with \verb|foldr|.

\section{Conclusions}

Anyone who has even just read a couple of blog posts about Haskell is
aware of space leaks.  This is where Haskell programs use more memory
than is necessary due to unevaluated thunks.  Well, there is a dual to
space leaks, I like to call time leaks.  A time leak is either:

\begin{itemize}
\item Doing work that is not necessary, or
\item Introducing increase complexity or code to avoid doing unnecessary
work.
\end{itemize}

I'm willing to be that many of you, reading the above examples, were
thinking to yourself something along the lines of ``You can totally do
that in my favorite strictly evaluated language-- all you need to do
is...'' followed by some code.  I'm also willing to be that your
proposed solution fits into one (or more) of the following categories:

\begin{itemize}
\item It increases the complexity of either the interface or the
implementation, or most likely both.  For example, ``Just pass in a flag
as to whether you need the result, the derivitive, or both''.
\item It increases the amount code.  ``Just write a specialized find the
k minimum elements function.''
\item It implements a non-optimizing version of lazy evaluation.  ``Just
use an iterator, and stop pulling items from it when you want to stop.''
\end{itemize}

What all the responses to the examples I gave have in common is that
they involve time leaks.  Now, space leaks are a problem in lazily
evaluated languages.  But due to their strangeness and uncommon nature
(at least in strictly evaluated languages), they loom large in the
imagination.  But they are not 1\% the problem that time leaks are,
especially in strictly evaluated languages.  But most programmers don't
notice the time leaks, for the same reason a fish doesn't notice water.  
It is almost the definition of a real world program that we have a
surfeit of complexity.  Any increase in either the size or complexity of
the code base is a negative.

It is almost the definition of a real world program that we have a
surfeit of complexity.  There is never enough time and never enough
hands to do the work that is needed, and no human brain is large enough
to hold even a medium sized program, in all it's details, in it at once.
Any increase in complexity should be anathema to any real world
programmer.  Which is what people miss about the entire functional
programming approach.  It's not about being elegant for elegance's sake.
We're not artists here.  But a well functioning maching (which includes
programs) always has an elegance about it.

\end{document}
